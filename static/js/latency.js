
$(document).ready(function () {
    namespace = '/flask';

    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);

    socket.on('connect', function () {
        $('#status').text('Connected');
    });

    socket.on('log', function (msg) {
        $('#log-view').prepend('<p>' + msg + '</p>').html();
    });

    var clear_message_timer;
    socket.on('message', function (msg) {
        $('#message').text(msg);

        if (clear_message_timer != null) {
            clearTimeout(clear_message_timer);
        }

        clear_message_timer = setTimeout(function () {
            $('#message').text("");
        }, 2000);
    });

    $('#start-button').click(function () {
        socket.emit('start');
    });

    $('#stop-button').click(function () {
        socket.emit('stop');
    });

    var ping_pong_times = [];
    var start_time;
    setInterval(function () {
        start_time = (new Date).getTime();
        socket.emit('measure');
    }, 1000);

    var latency_display_timer;
    socket.on('pong', function (msg) {
        var latency = (new Date).getTime() - start_time;
        ping_pong_times.push(latency);
        ping_pong_times = ping_pong_times.slice(-30);  // Последние 30 записей

        var sum = 0;
        for (let i = 0; i < ping_pong_times.length; i++) {
            sum += ping_pong_times[i];
        }
        $('#ping-pong').text(Math.round(10 * sum / ping_pong_times.length) / 10 + " ms");
        $('#status').text('Connected');
        if (latency_display_timer != null) {
            clearTimeout(latency_display_timer);
        }
        latency_display_timer = setTimeout(display_latency_timeout, 1000);
    })

    function display_latency_timeout() {
        $('#ping-pong').text('-- ');
        $('#status').text('Disconnected')
    }
})
