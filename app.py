from time import sleep
from datetime import datetime
from threading import Thread
from queue import Queue
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
from flask_bootstrap import Bootstrap


app = Flask(__name__)
sio = SocketIO(app)
bootstrap = Bootstrap(app)

namespace = '/flask'

colors = {
    'red': 128,
    'green': 128,
    'blue': 128
}


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Заглавная страница', colors=colors)


@app.route('/remote')
def remote():
    return render_template('remote.html', colors=colors, title='Remote')


@sio.on('colors', namespace=namespace)
def colors_sio(msg):
    global colors
    colors[msg['color']] = msg['value']
    sio.emit('update_sliders', msg, namespace=namespace)
    sio.emit('square', colors, namespace=namespace)


@app.route('/sliders')
def sliders():
    return render_template('sliders.html', title='Тестовая Bootstrap', colors=colors)


@app.route('/latency')
def latency():
    return render_template('latency.html', async_mode=sio.async_mode)


thread = None
running = False
qtask = None
q = Queue()


def worker_task():
    global running
    print('Thread started')

    while running:
        sleep(5)
        q.put(str(datetime.now()))
    print('Thread stopped')


def queue_task():
    count = 0
    while True:
        sio.sleep(0.5)
        count += 1
        if not q.empty():
            item = q.get()
            print(item)
            q.task_done()
            sio.emit('log', item, namespace=namespace)


@app.before_request
def init_job():
    print('@app.before_request')
    global qtask
    qtask = sio.start_background_task(queue_task)


@sio.on('start', namespace=namespace)
def start():
    global thread
    global running
    if thread is not None:
        if thread.is_alive():
            sio.emit('message', 'Thread is active', namespace=namespace)
            return

    thread = Thread(target=worker_task)
    thread.daemon = True
    running = True
    sio.emit('message', 'Thread created', namespace=namespace)
    thread.start()


@sio.on('stop', namespace=namespace)
def stop():
    global thread
    global running
    running = False
    if thread is not None:
        if thread.is_alive():
            print('Told the thread to stop')
            sio.emit('message', 'Told the thread to stop', namespace=namespace)
            return
    sio.emit('message', 'Thread is not running', namespace=namespace)


@sio.on('connect', namespace=namespace)
def connect():
    global qtask
    if qtask is None:
        qtask = sio.start_background_task(queue_task)
    print('Client connected', request.sid)
    emit('connect')


@sio.on('disconnect', namespace=namespace)
def disconnect():
    print('Client disconnected')
    emit('disconnect')


@sio.on('measure', namespace=namespace)
def ping_pong():
    emit('pong')


if __name__ == '__main__':
    sio.run(app, debug=True)
